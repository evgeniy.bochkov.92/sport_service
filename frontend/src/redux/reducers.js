import Types from "./types";
const initialState = {
    teams: [],
    matches: [],
    messages: [],
    loading:false
};

const matchReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.FUTURE_MATCHES_LOADING: {
            return {...state,loading: action.payload};
        }

        case Types.GET_FUTURE_MATCHES: {
            return {...state,matches: action.payload};
        }

        case Types.PAST_MATCHES_LOADING: {
            return {...state,loading: action.payload};
        }

        case Types.GET_PAST_MATCHES: {
            return {...state,matches: action.payload};
        }

        case Types.ALL_MATCHES_LOADING: {
            return {...state,loading: action.payload};
        }

        case Types.GET_ALL_MATCHES: {
            return {...state,matches: action.payload};
        }

        case Types.TEXT_TRANSLATION_MESSAGES_LOADING: {
            return {...state,loading: action.payload};
        }

        case Types.GET_TEXT_TRANSLATION_MESSAGES: {
            return {...state,messages: action.payload};
        }

        case Types.CLEAR_TEXT_TRANSLATION_MESSAGES: {
            return {...state,messages: action.payload};
        }

        case Types.TEAMS_LOADING: {
            return {...state,loading: action.payload};
        }

        case Types.GET_TEAMS: {
            return {...state,teams: action.payload};
        }

        default:
            return state;
    }
}

export default matchReducer;