import Types from "./types";
import axios from "axios";
const API_URL = 'http://localhost:8000';

export const getTeams = () => {
    return dispatch => {
        dispatch({type:Types.TEAMS_LOADING, payload:true})
        axios.get(`${API_URL}/api/teams/`)
            .then(response => {
                    dispatch({type:Types.GET_TEAMS, payload:response.data})
                }
            )
            .catch(err => {
                    console.log(err)
                    dispatch({type:Types.TEAMS_LOADING, payload:false})
            }
            );
    }
}

export const getFutureMatches = () => {
    return dispatch => {
        dispatch({type:Types.FUTURE_MATCHES_LOADING, payload:true})
        axios.get(`${API_URL}/api/future-matches/`)
            .then(response => {
                    dispatch({type:Types.GET_FUTURE_MATCHES, payload:response.data})
                }
            )
            .catch(err => {
                    console.log(err)
                    dispatch({type:Types.FUTURE_MATCHES_LOADING, payload:false})
            }
            );
    }
}

export const getPastMatches = () => {
    return dispatch => {
        dispatch({type:Types.PAST_MATCHES_LOADING, payload:true})
        axios.get(`${API_URL}/api/past-matches/`)
            .then(response => {
                    dispatch({type:Types.GET_PAST_MATCHES, payload:response.data})
                }
            )
            .catch(err => {
                    console.log(err)
                    dispatch({type:Types.PAST_MATCHES_LOADING, payload:false})
            }
            );
    }
}

export const getAllMatches = () => {
    return dispatch => {
        dispatch({type:Types.ALL_MATCHES_LOADING, payload:true})
        axios.get(`${API_URL}/api/all-matches/`)
            .then(response => {
                    dispatch({type:Types.GET_ALL_MATCHES, payload:response.data})
                }
            )
            .catch(err => {
                    console.log(err)
                    dispatch({type:Types.ALL_MATCHES_LOADING, payload:false})
            }
            );
    }
}

export const getMessages = (pk) => {
    return dispatch => {
        dispatch({type:Types.TEXT_TRANSLATION_MESSAGES_LOADING, payload:true})
        axios.get(`${API_URL}/api/text-translations/${pk}/`)
            .then(response => {
                    dispatch({type:Types.GET_TEXT_TRANSLATION_MESSAGES, payload:response.data.messages})
                }
            )
            .catch(err => {
                    console.log(err)
                    dispatch({type:Types.TEXT_TRANSLATION_MESSAGES_LOADING, payload:false})
                    dispatch({type:Types.CLEAR_TEXT_TRANSLATION_MESSAGES, payload:[]})
            }
            );
    }
}