import React from "react";
import { Switch, Route, Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import "antd/dist/antd.css";
import TextTranslation from "./TextTranslation";
import CalendarMatches from "./CalendarMatches";
import ListTeams from "./ListTeams";
import styles from './styles.css';


class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected:1
        }
    }
    handleClick = e => {
        this.setState({selected:e})
    }
    render() {
        const { Header, Content } = Layout;

        return (
            <Layout>
                <Header>
                    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[this.state.selected.toString()]}>
                        <Menu.Item key="1"><Link to="/"> Календарь Матчей </Link></Menu.Item>
                        <Menu.Item key="2"><Link to="/teams"> Список Команд </Link></Menu.Item>
                    </Menu>
                </Header>
                <Content>
                    <Switch>
                        <Route path="/text-translation/:pk" component={TextTranslation}/>
                        <Route path="/teams" component={ListTeams}/>
                        <Route path="/" component={CalendarMatches}/>
                    </Switch>
                </Content>
            </Layout>
        );
    }
}


export default Main