import React from 'react';
import {connect} from 'react-redux'
import {getPastMatches} from '../redux/actions.js'
import {DayTitle, DayMatches, InfoTeam, Score, CardMatch} from './TableMatchesExtras.js'


class TablePastMatches extends React.Component {

    componentDidMount() {
        this.props.getPastMatches();
    }

    render() {
        const blocks = []
        const matches = this.props.matches

        matches.forEach((day) => {
            blocks.push(
                <DayTitle date={day.date} key={day.date}/>
            );
            blocks.push(
                <DayMatches matches={day.matches} key={`${day.date}_matches`}/>
            );
        })

        return (
            <div className="tab-calendar" id="tab-calendar-future">
                <div className="b-wide_block">
                    {blocks}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    matches: state.matches
})

const mapDispatchToProps = {
    getPastMatches
}

export default connect(mapStateToProps, mapDispatchToProps)(TablePastMatches)