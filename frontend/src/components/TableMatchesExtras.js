import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from 'react-router-dom';

export class DayTitle extends React.Component {
    render(){
        const date = this.props.date;
        return (
            <div className="b-final_cup_date">
                <span className="e-bevel">Регулярный чемпионат</span>
                <b>{date}</b>
                <b>|</b><small>Неделя 4</small>
            </div>
        );
    }
}


export class DayMatches extends React.Component {
    render(){
        const blocks = [];
        const matches = this.props.matches;
        matches.forEach((match) => {
            blocks.push(
                <CardMatch match={match} key={match.pk}/>
            );
        });
        return (
            <div className="m-future">
                <ul className="b-wide_tile">
                    {blocks}
                </ul>
            </div>
        );
    }
}


export class InfoTeam extends React.Component {
    render(){
        const team = this.props.team;
        return (
            <dl className="b-details m-club">
                <dt className="e-details_img">
                    <img src={`data:image/png;base64,${team.logo}`} alt="" />
                </dt>
                <dd className="b-details_txt">
                    <h5 className="e-club_name">
                        <a href="#">{team.name}</a>
                    </h5>
                    <p className="e-club_sity">{team.hometown}</p>
                </dd>
            </dl>
        );
    }
}


export class Score extends React.Component {
    render(){
        const time = this.props.time;
        const place = this.props.place;
        const host_team_score=this.props.host_team_score
        const guest_team_score=this.props.guest_team_score
        const h3 = host_team_score && guest_team_score?<h3>{host_team_score} — {guest_team_score}</h3>:<h3>{time} <small>мск</small></h3>
        return (
            <dl className="b-score">
                <dt className="b-total_score">
                    <b className="e-match-num">№ 119</b>
                    {h3}
                </dt>
                <dd className="b-period_score">
                    <p className="e-club_sity">{place}</p>
                </dd>
            </dl>
        );
    }
}


export class CardMatch extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            className:'b-title-option',
            display:'none'
        }
    }
    onMouseOver()
    {
        if (this.state.className === 'b-title-option'){
            this.setState({ className: 'b-title-option s-option-show' });
            this.setState({ display: 'block' });
        }
    }
    onMouseOut()
    {
        if (this.state.className === 'b-title-option s-option-show'){
            this.setState({ className: 'b-title-option' });
            this.setState({ display: 'none' });
        }
    }
    render(){
        const match = this.props.match;
        return (
            <li className="b-wide_tile_item" onMouseOver={this.onMouseOver.bind(this)} onMouseOut={this.onMouseOut.bind(this)}>
                <InfoTeam team={match.host_team} key={match.host_team.pk}/>
                <dl className={this.state.className} style={{display: this.state.display}}>
                    <div className="b-title-option-wrapper">
                        <div className="b-title-option-inner">
                            <ul>
                                <li><Link to={`/text-translation/${match.pk}`}> Текст </Link></li>
                                <li><a href="#">Live</a></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </dl>
                <Score time={match.time} place={match.place} host_team_score={match.host_team_score} guest_team_score={match.guest_team_score} key={match.place}/>
                <InfoTeam team={match.guest_team} key={match.guest_team.pk}/>
            </li>
        );
    }
}