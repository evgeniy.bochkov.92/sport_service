import React, { Component } from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import { Route, Link } from 'react-router-dom'
import './App.css';

import Main from "./Main";
import { Provider as ReduxProvider } from "react-redux";
import configureStore from "../redux/store";

const reduxStore = configureStore(window.REDUX_INITIAL_DATA);

const BaseLayout = () => (
<div className="container-fluid">
    <Main />
</div>
)

class App extends Component {
  render() {
    return (
      <ReduxProvider store={reduxStore}>
        <BrowserRouter>
            <BaseLayout/>
        </BrowserRouter>
      </ReduxProvider>
    );
  }
}

export default App;

const container = document.getElementById("app");
render(<App />, container);