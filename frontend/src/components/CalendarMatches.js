import React from "react";
import TableFutureMatches from "./TableFutureMatches";
import TablePastMatches from "./TablePastMatches";
import TableAllMatches from "./TableAllMatches";
import styles from './styles.css';


class CalendarMatches extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected:1
        }
    }
    handleClick = e => {
        this.setState({selected:e})
    }
    render() {
        return (
            <div className="b-content_section m-stats s-float_panel_start">
                <div className="b-wide_block m-not_pad">
                    <div className="b-title_cover">
                        <h2>Матчи</h2>
                        <div className="b-title_tab_cover">
                            <div className="m-scroll_block ui-draggable" data-tab="calendar">
                                <span className="e-tab_subtitle">Отображение:</span>
                                <b className={`e-title_tab_item ${this.state.selected.toString()==1?'m-active':''}`} key="1" onClick={() => this.handleClick(1)} data-id="future">текущие и будущие</b>
                                <b className={`e-title_tab_item ${this.state.selected.toString()==2?'m-active':''}`} key="2" onClick={() => this.handleClick(2)} data-id="last">прошедшие</b>
                                <b className={`e-title_tab_item ${this.state.selected.toString()==3?'m-active':''}`} key="3" onClick={() => this.handleClick(3)} data-id="all">все</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="m-top_panel">
                    {this.state.selected==1?<TableFutureMatches />:
                    this.state.selected==2?<TablePastMatches />:
                    this.state.selected==3?<TableAllMatches />:<TableFutureMatches />
                    }
                </div>
            </div>
        );
    }
}


export default CalendarMatches