import React from 'react';
import {connect} from 'react-redux'
import {getMessages} from '../redux/actions.js'


class Message extends React.Component {
    render(){
        const created_at = this.props.created_at;
        const text = this.props.text;
        return (
            <li className="b-text_translation_item">
                <div className="b-center_scale_point">
                    <span className="b-small_round m-txt">
                        <span className="e-event_time">{created_at}</span>
                    </span>
                </div>
                <div className="b-right m-txt">
                    <div className="b-txt_block">
                        <p className="e-action_txt">{text}</p>
                    </div>
                </div>
            </li>
        );
    }
}


class TextTranslation extends React.Component {
    intervalID;

    componentDidMount() {
        const { match: { params } } = this.props;
        this.props.getMessages(params.pk);

        this.intervalID = setInterval(this.props.getMessages.bind(this, params.pk), 10000);
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
      }

    render() {
        const rows = []
        const messages = this.props.messages

        messages.forEach((message) => {
            rows.push(
                <Message created_at={message.created_at} text={message.text} key={message.pk}/>
            );
        })

        return (
            <div className="b-content_section m-text_translation" style={{margin: "0"}}>
                <div className="b-wide_block">
                    <div className="b-title_cover">
                        <h2>Текстовая трансляция</h2>
                        <div className="b-title_tab_cover">
                            <div className="m-scroll_block" data-tab="game">
                                <b className="e-title_tab_item m-active" data-id="events">ход матча</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="b-wide_block m-center tab-game" id="tab-game-events">
                    <div className="b-text_translation_cover b-text_translation_list_cover">
                        <ul className="b-text_translation b-text_translation_list">
                            {rows}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    messages: state.messages
})

const mapDispatchToProps = {
    getMessages
}

export default connect(mapStateToProps, mapDispatchToProps)(TextTranslation)