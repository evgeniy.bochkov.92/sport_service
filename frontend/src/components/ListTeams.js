import React from 'react';
import {connect} from 'react-redux'
import {getTeams} from '../redux/actions.js'
import {Card, Row, Col, message } from 'antd';
import { EditOutlined, EllipsisOutlined, DeleteOutlined } from '@ant-design/icons';
import "antd/dist/antd.css";


class ListTeams extends React.Component {

    componentDidMount() {
        this.props.getTeams();
    }

    render() {
        const teams = this.props.teams
        console.log(teams)

        return (
            <div className="teams--list">
                <table className="table">
                <thead key="thead">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Logo</th>
                </tr>
                </thead>
                <tbody>
                {teams.map(t => (
                    <tr key={t.pk}>
                    <td>{t.pk}</td>
                    <td>{t.name}</td>
                    <td><img src={`data:image/png;base64,${t.logo}`} /></td>
                </tr>))}
                </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    teams: state.teams
})

const mapDispatchToProps = {
    getTeams
}

export default connect(mapStateToProps, mapDispatchToProps)(ListTeams)