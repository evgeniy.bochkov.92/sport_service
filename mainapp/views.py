import datetime

from django.http import JsonResponse
from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from mainapp.models import Team, Match, TextTranslation
from mainapp.serializers import TeamSerializer, MatchSerializer, TextTranslationSerializer


def index(request):
    return render(request, "mainapp/index.html")


class TeamViewSet(ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer


class FutureMatchViewSet(ModelViewSet):
    queryset = Match.objects.filter(date__gte=datetime.date.today()).order_by('date').values('date').distinct()
    serializer_class = MatchSerializer


class PastMatchViewSet(ModelViewSet):
    queryset = Match.objects.filter(date__lt=datetime.date.today()).order_by('date').values('date').distinct()
    serializer_class = MatchSerializer


class AllMatchViewSet(ModelViewSet):
    queryset = Match.objects.order_by('date').values('date').distinct()
    serializer_class = MatchSerializer


class TextTranslationViewSet(ModelViewSet):
    queryset = TextTranslation.objects.all()
    serializer_class = TextTranslationSerializer
