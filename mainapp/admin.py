from django.contrib import admin
from django import forms

from .models import Team, Match, TextTranslation, Message


class TeamLogoForm(forms.ModelForm):
    logo = forms.FileField(required=False)

    def save(self, commit=True):
        if self.cleaned_data.get('logo') is not None \
                and hasattr(self.cleaned_data['logo'], 'file'):
            data = self.cleaned_data['logo'].file.read()
            self.instance.logo = data
        return self.instance

    def save_m2m(self):
        # FIXME: this function is required by ModelAdmin, otherwise save process will fail
        pass


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    fields = ('id', 'name', 'logo', 'logo_image', 'hometown')
    form = TeamLogoForm
    readonly_fields = ('id', 'logo_image',)
    list_display = ('id', 'name', 'logo_image')
    list_display_links = ('name',)
    ordering = ('name',)


@admin.register(Match)
class MatchAdmin(admin.ModelAdmin):
    fields = ('id', 'host_team', 'host_team_score', 'guest_team', 'guest_team_score', 'date', 'time', 'place')
    readonly_fields = ('id',)
    list_display = ('id', 'host_team', 'guest_team', 'date', 'time', 'place')
    ordering = ('date',)


class MessageInline(admin.TabularInline):
    model = Message
    ordering = ('pk',)


@admin.register(TextTranslation)
class TextTranslationAdmin(admin.ModelAdmin):
    fields = ('match',)
    inlines = [
        MessageInline,
    ]


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    fields = ('id', 'text', 'text_translation')
    readonly_fields = ('id', 'created_at', 'text_translation')
    list_display = ('id', 'text', 'created_at', 'text_translation')
    ordering = ('created_at',)
