from django.urls import path, re_path, include
from rest_framework.routers import DefaultRouter

from mainapp import views
from mainapp.views import TeamViewSet, AllMatchViewSet, FutureMatchViewSet, PastMatchViewSet, TextTranslationViewSet

router = DefaultRouter()
router.register("teams", TeamViewSet)
router.register("matches", AllMatchViewSet)
router.register("future-matches", FutureMatchViewSet)
router.register("past-matches", PastMatchViewSet)
router.register("all-matches", AllMatchViewSet)
router.register("text-translations", TextTranslationViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('', views.index),
    re_path(r'^text-translation/\d+/', views.index),
    re_path(r'^teams', views.index),
]