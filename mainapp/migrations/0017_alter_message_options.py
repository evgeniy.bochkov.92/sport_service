# Generated by Django 3.2.7 on 2021-09-27 10:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0016_alter_message_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ('-pk',)},
        ),
    ]
