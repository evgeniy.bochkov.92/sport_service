# Generated by Django 3.2.7 on 2021-09-25 15:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0005_auto_20210925_2040'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='hometown',
            field=models.CharField(default='', max_length=30, verbose_name='Город'),
        ),
    ]
