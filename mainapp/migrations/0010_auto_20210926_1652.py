# Generated by Django 3.2.7 on 2021-09-26 11:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0009_auto_20210926_1649'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='guest_team_score',
            field=models.IntegerField(blank=True, verbose_name='Счет гостевой команды'),
        ),
        migrations.AlterField(
            model_name='match',
            name='host_team_score',
            field=models.IntegerField(blank=True, verbose_name='Счет принимающей команды'),
        ),
    ]
