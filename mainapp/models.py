from django.db import models
from django.utils.safestring import mark_safe


class Team(models.Model):
    name = models.CharField("Название команды", max_length=30)
    logo = models.BinaryField("Логотип команды", default=b'\x08', editable=True)
    hometown = models.CharField("Город", default='', max_length=30)

    def logo_image(self):
        from base64 import b64encode
        return mark_safe('<img src = "data: image/png; base64, {}" width="200" height="200">'.format(
            b64encode(self.logo).decode('utf8')
        ))

    logo_image.short_description = 'Image'
    logo_image.allow_tags = True

    def __str__(self):
        return self.name


class Match(models.Model):
    host_team = models.ForeignKey(Team, on_delete=models.PROTECT, related_name="host_matches")
    guest_team = models.ForeignKey(Team, on_delete=models.PROTECT, related_name="guest_matches")
    date = models.DateField("Дата матча", default="2021-01-01")
    time = models.TimeField("Время начала матча", default="00:00")
    place = models.CharField("Место проведения матча", default='', max_length=30, blank=True)
    host_team_score = models.IntegerField("Счет принимающей команды", blank=True, null=True)
    guest_team_score = models.IntegerField("Счет гостевой команды", blank=True, null=True)

    class Meta:
        verbose_name = "Match"
        verbose_name_plural = "Matches"

    def __str__(self):
        return f"{self.host_team} – {self.guest_team}"

    def save(self, *args, **kwargs):
        if not self.place:
            self.place = self.host_team.hometown
        super(Match, self).save(*args, **kwargs)


class TextTranslation(models.Model):
    match = models.OneToOneField(Match, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return f"{self.match.host_team} – {self.match.guest_team}"


class Message(models.Model):
    text = models.CharField("Текст сообщения", max_length=255)
    created_at = models.TimeField("Дата создания", auto_now_add=True)
    text_translation = models.ForeignKey(TextTranslation, on_delete=models.CASCADE, related_name='messages')

    class Meta:
        ordering = ('-pk',)

