from rest_framework import serializers

from mainapp.models import Team, Match, TextTranslation, Message


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = "pk", "name", "logo", "hometown"
        view_name = "teams"


class MatchFieldSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    time = serializers.TimeField(format="%H:%M")
    place = serializers.CharField()
    host_team = TeamSerializer()
    guest_team = TeamSerializer()
    host_team_score = serializers.IntegerField()
    guest_team_score = serializers.IntegerField()


class MatchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Match
        fields = "date", "matches"

    matches = serializers.SerializerMethodField('get_match')

    def get_match(self, obj):
        matches = Match.objects.filter(date=obj['date'])
        match_serializer = MatchFieldSerializer(matches, many=True)
        return match_serializer.data


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "pk", "text", "created_at"

    created_at = serializers.TimeField(format="%H:%M")


class TextTranslationSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True, required=False)

    class Meta:
        model = TextTranslation
        fields = "match", "messages"
